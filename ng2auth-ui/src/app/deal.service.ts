import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {Observable} from "rxjs/Observable";
import {AuthService} from "./auth/auth.service";

@Injectable()
export class DealService {

  private publicDealsUrl = "http://localhost:3001/api/deals/public";
  private privateDealsUrl = "http://localhost:3001/api/deals/private";

  constructor(private http: HttpClient,
              private authService: AuthService) {
  }

  getPublicDeals() {
    return this.http
      .get(this.publicDealsUrl)
      .pipe(
        catchError(this.handleError)
      );
  }

  getPrivateDeals() {
    return this.http
      .get(this.privateDealsUrl, {
        headers: new HttpHeaders().set("Authorization", `Bearer $(this.authService.accessToken}`)
      })
      .pipe(
        catchError(this.handleError)
      );
  }

  purchase(item) {
    alert(`You bought the: ${item.name}`);
  }

  private handleError(err: HttpErrorResponse | any) {
    console.error('An error occurred', err);
    return Observable.throw(err.message || err);
  }
}

