import {Injectable} from '@angular/core';
import * as auth0 from 'auth0-js';
import {Auth0DecodedHash, Auth0UserProfile} from 'auth0-js';
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {

  auth0 = new auth0.WebAuth({
    clientID: environment.auth.clientID,
    domain: environment.auth.domain,
    responseType: 'token',
    redirectUri: environment.auth.redirect,
    audience: environment.auth.audience,
    scope: environment.auth.scope
  });

  userProfile: any;
  accessToken: string;
  authenticated: boolean;

  constructor(private router: Router) {
    this.getAccessToken();
  }

  get isLoggedIn() {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return Date.now() < expiresAt && this.authenticated;
  }

  login() {
    this.auth0.authorize();
  }

  logout() {
    localStorage.removeItem('expires_at');
    this.userProfile = undefined;
    this.accessToken = undefined;
    this.authenticated = false;
  }

  handleLoginCallback() {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken) {
        window.location.hash = '';
        this.getUserInfo(authResult);
      } else if (err) {
        console.error(`Error ${err.error}`);
      }
      this.router.navigate(['/']);
    })
  }

  getAccessToken() {
    this.auth0.checkSession({}, (err, authResult) => {
      if (authResult && authResult.accessToken) {
        this.getUserInfo(authResult)
      } else if (err) {
        console.log(err);
        this.logout();
        this.authenticated = false;
      }
    })
  }

  getUserInfo(authResult: Auth0DecodedHash) {
    this.auth0.client.userInfo(authResult.accessToken, (err, profile) => {
      if (profile) {
        this._setSession(authResult, profile)
      }
    })
  }

  private _setSession(authResult: Auth0DecodedHash, profile: Auth0UserProfile) {
    const expTime = authResult.expiresIn * 1000 + Date.now();
    localStorage.setItem('expires_at', JSON.stringify(expTime));
    this.accessToken = authResult.accessToken;
    this.userProfile = profile;
    this.authenticated = true;
  }
}
